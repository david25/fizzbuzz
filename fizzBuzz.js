function fizzBuzz() {
  let fizzBuzzArray = [];

  for (var index = 1; index < 101; index++) {
    if (index % 15 === 0) {
      fizzBuzzArray.push("FizzBuzz");
    } else if (index % 3 === 0) {
      fizzBuzzArray.push("Fizz");
    } else if (index % 5 === 0) {
      fizzBuzzArray.push("Buzz");
    } else {
      fizzBuzzArray.push(index);
    }
  }

  console.log(fizzBuzzArray.join(" "));
}

fizzBuzz();
