function barBaz({
  firstValue,
  secondValue,
  firstString,
  secondString,
  firstLogic,
  secondLogic,
  limit,
}) {
  const barBazzArray = [];
  const thirdValue = firstValue * secondValue;
  const thirdString = `${firstString}${secondString}`;

  console.log(`${thirdString}:`);

  for (let index = 1; index < limit; index++) {
    if (firstLogic(index) && secondLogic(index)) {
      barBazzArray.push(thirdString);
    } else if (firstLogic(index)) {
      barBazzArray.push(firstString);
    } else if (secondLogic(index)) {
      barBazzArray.push(secondString);
    } else {
      barBazzArray.push(index);
    }
  }

  console.log(barBazzArray.join(" "));
}

barBaz({
  firstString: "Fizz",
  secondString: "Buzz",
  firstLogic: (index) => index % 3 === 0,
  secondLogic: (index) => index % 5 === 0,
  limit: 101,
});

barBaz({
  firstString: "Bar",
  secondString: "Baz",
  firstLogic: (index) => index % 7 === 0,
  secondLogic: (index) => index % 11 === 0,
  limit: 101,
});

barBaz({
  firstString: "Fizz",
  secondString: "Buzz",
  firstLogic: (index) => index >= 3 && index <= 6,
  secondLogic: (index) => index % 10 === 5,
  limit: 101,
});
