async function barBaz({
  firstValue,
  secondValue,
  firstString,
  secondString,
  firstLogic,
  secondLogic,
  limit,
}) {
  const barBazzArray = [];
  const thirdValue = firstValue * secondValue;
  const thirdString = `${firstString}${secondString}`;

  console.log(`${thirdString}:`);

  for (let index = 1; index < limit; index++) {
    if ((await firstLogic(index)) && secondLogic(index)) {
      barBazzArray.push(thirdString);
    } else if (await firstLogic(index)) {
      barBazzArray.push(firstString);
    } else if (secondLogic(index)) {
      barBazzArray.push(secondString);
    } else {
      barBazzArray.push(index);
    }
  }

  console.log(barBazzArray.join(" "));
}

let numberIsInTable = (number) => {
  const numbersInTable = [5, 10, 99];
  return new Promise((resolve, reject) => {
    const found = numbersInTable.includes(number);

    if (found) {
      resolve(true);
    }

    resolve(false);
  });
};

(async () =>
  await barBaz({
    firstString: "Fizz",
    secondString: "Buzz",
    firstLogic: (index) => numberIsInTable(index),
    secondLogic: (index) => index >= 3 && index <= 6,
    limit: 101,
  }))();
