function barBaz({ firstValue, secondValue, firstString, secondString, limit }) {
  const barBazzArray = [];
  const thirdValue = firstValue * secondValue;
  const thirdString = `${firstString}${secondString}`;

  console.log(`${thirdString}:`);
  for (let index = 1; index < limit; index++) {
    if (index % thirdValue === 0) {
      barBazzArray.push(thirdString);
    } else if (index % firstValue === 0) {
      barBazzArray.push(firstString);
    } else if (index % secondValue === 0) {
      barBazzArray.push(secondString);
    } else {
      barBazzArray.push(index);
    }
  }

  console.log(barBazzArray.join(" "));
}

barBaz({
  firstValue: 3,
  secondValue: 5,
  firstString: "Fizz",
  secondString: "Buzz",
  limit: 101,
});

barBaz({
  firstValue: 7,
  secondValue: 11,
  firstString: "Bar",
  secondString: "Baz",
  limit: 101,
});
